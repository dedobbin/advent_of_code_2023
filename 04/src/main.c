#include <stdio.h>
#include <string.h>
#include <assert.h>

int main (int argc, char **argv)
{
    const char* const input_path = argc > 1 ? argv[1] : "input.txt";

    FILE* fp = fopen(input_path, "r");
    if (fp == NULL){
        perror("Could not open input");
        return 1;
    }

    int total = 0;
    char card[300];
    while (fgets(card, sizeof(card), fp) != NULL){
        static int card_id = 0;
        card_id ++;
        int start_index = strstr(card, ":") - card+2;
        char* tmp =  strstr(card, "|");
        int sep_index = tmp - card;
        int l_end = sep_index-1;
        int r_start = sep_index+1;
        int end = strlen(card)-1;

        // Grab my numbers
        char my_numbers_str[200];
        memcpy(my_numbers_str, card+start_index, l_end-start_index);
        int my_numbers[40];
        int n_my_numbers = 0;
        char* token = strtok(my_numbers_str, " ");
        while(token != NULL){
            int tmp = atoi(token);
            my_numbers[n_my_numbers++] = tmp;
            printf("%d ", tmp);
            token = strtok(NULL, " ");
        }
        printf("\n");

        // Grab winning numbers
        char winning_numbers_str[200];
        memcpy(winning_numbers_str, card+r_start, end - r_start+1);

        int score = 0;
        token = strtok(winning_numbers_str, " ");
        while(token != NULL){
            int winning = atoi(token);
            for (int i = 0;i < n_my_numbers; i++){
                if (winning == my_numbers[i]){
                    score = score == 0 ? 1 : score * 2; 
                }
            }
            token = strtok(NULL, " ");
        }
        total += score;

    }
    assert(total == 21158);
    return 0;
}
