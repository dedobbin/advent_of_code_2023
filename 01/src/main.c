#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

int part_one(FILE* fp)
{
    char str[100];
    int total = 0;
    while(fgets(str, sizeof(str), fp)){
        str[strlen(str)-1] = '\0';
        char l = -1;
        char r = -1;
        
        for (unsigned int i = 0; i < strlen(str); i++){
            if (l < 0 && isdigit(str[i])){
                l = str[i];
            }

            if (r < 0 && isdigit(str[strlen(str)-1-i])){
                r = str[strlen(str)-1-i];
            }

            if (l >= 0 && r >=0){
                break;
            }
        }
        char res_str[3] = {l, r, '\0'};
        int res = atoi(res_str);
        total += res;
    }

    return total;
}

int part_two(FILE* fp)
{
    char digits[9][100] = {"one","two","three","four","five","six","seven","eight","nine"};

    char str[100];
    int total = 0;
    while(fgets(str, sizeof(str), fp)){
        char l = -1; 
        char r = -1; 
        str[strlen(str)-1] = '\0';
        for (unsigned i = 0; i < strlen(str); i++){            
            for (int j = 0; j < 9; j++){
                if (l < 0){
                    if (isdigit(str[i])){
                        l = str[i];
                    }
                    else if (strncmp(str+i, digits[j], strlen(digits[j])) == 0){
                        l = j + 1 + '0';
                    }
                }
                if (isdigit(str[i])){
                    r = str[i];
                }
                else if (strncmp(str+i, digits[j], strlen(digits[j])) == 0){
                    r = j + 1 + '0';
                }
            }
        }
        char res_str[3] = {l, r, '\0'};
        int res = atoi(res_str);
        total += res;
    }
    return total;
}

int main(int argc, char* argv[]) {
    FILE* fp = fopen(argc > 1 ? argv[1] : "input.txt", "r");
    if (fp == NULL) {
        perror("Could not open input");
        exit(1);
    }

    int one = part_one(fp);
    printf("Part one: %d\n", one);
    assert(one == 54331);

    rewind(fp);
    int two = part_two(fp);
    printf("Part two: %d\n", two);
    assert(two == 54518);

    fclose(fp);
}