#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <limits.h>

int part_one(FILE* fp)
{
    int total = 0;
    int rgb_allowed['r'+1];
    rgb_allowed['r'] = 12;
    rgb_allowed['g'] = 13;
    rgb_allowed['b'] = 14;

   char line[500];
   while(fgets(line, sizeof(line), fp) != NULL){  
        char* line_cpy = strdup(line);
        char* token = strtok(line_cpy, " ");
        int num_game;
        while (token != NULL){
            token[strlen(token)-1] = '\0';
            
            int tmp = atoi(token);
            if (tmp != 0){
                num_game = tmp;
                //printf("Game: %s\n", token);
                break;
            }
            token = strtok(NULL, " ");
        }

        char *position = strchr(line, ':');
        int index = position - line + 1;

        token = strtok(line+index, ", ;");
        while (token != NULL){
            int num = atoi(token);
            token = strtok(NULL,", ;");
            if (num > rgb_allowed[token[0]]){
                num_game = 0;
                break;
            }
            token = strtok(NULL," ");
        }
        total += num_game;

   }
    return total;
}

int part_two(FILE* fp)
{
    int total = 0;

    char line[400];
    while(fgets(line, sizeof(line), fp)!= NULL){
        unsigned int rgb_highest['r'+1];
        rgb_highest['r'] = 0;
        rgb_highest['g'] = 0;
        rgb_highest['b'] = 0;

        char* line_cpy = strdup(line);
        char* token = strtok(line_cpy, " ");
        int num_game;
        while (token != NULL){
            token[strlen(token)-1] = '\0';
            
            int tmp = atoi(token);
            if (tmp != 0){
                num_game = tmp;
                break;
            }
            token = strtok(NULL, " ");
        }

        char *position = strchr(line, ':');
        int index = position - line + 1;

        token = strtok(line+index, ", ;");
        while (token != NULL){
            unsigned int num = atoi(token);
            token = strtok(NULL,", ;");
            if (num > rgb_highest[token[0]] || rgb_highest[token[0]] == 0){
                rgb_highest[token[0]] = num;
            }
            
            token = strtok(NULL," ");
        }
        int power = rgb_highest['r'] * rgb_highest['g'] * rgb_highest['b'];
        total += power;
    }


    return total;
}

void combined(FILE* fp)
{
    int total_a = 0;
    int total_b = 0;

    int total = 0;
    int rgb_allowed['r'+1];
    rgb_allowed['r'] = 12;
    rgb_allowed['g'] = 13;
    rgb_allowed['b'] = 14;

   char line[500];
   while(fgets(line, sizeof(line), fp) != NULL){  
        unsigned int rgb_highest['r'+1];
        rgb_highest['r'] = 0;
        rgb_highest['g'] = 0;
        rgb_highest['b'] = 0;

        char* line_cpy = strdup(line);
        char* token = strtok(line_cpy, " ");
        int num_game;
        while (token != NULL){
            token[strlen(token)-1] = '\0';
            
            int tmp = atoi(token);
            if (tmp != 0){
                num_game = tmp;
                //printf("Game: %s\n", token);
                break;
            }
            token = strtok(NULL, " ");
        }

        char *position = strchr(line, ':');
        int index = position - line + 1;

        token = strtok(line+index, ", ;");
        while (token != NULL){
            int num = atoi(token);
            token = strtok(NULL,", ;");
            if (num > rgb_allowed[token[0]]){
                num_game = 0;
            }

            if (num > rgb_highest[token[0]] || rgb_highest[token[0]] == 0){
                rgb_highest[token[0]] = num;
            }

            token = strtok(NULL," ");
        }
        int power = rgb_highest['r'] * rgb_highest['g'] * rgb_highest['b'];
        total_b += power;
        total_a += num_game;

   }


    printf("Total a: %d\nTotal b: %d\n", total_a, total_b);
}

int main(int argc, char* argv[])
{
    const char* input_path = argc > 1 ? argv[1] : "input.txt";
    FILE* fp = fopen(input_path, "r");
    if (fp == NULL){
        perror("Failed to open input");
        exit(1);
    }

    // combined(fp);

    int one = part_one(fp);
    printf("Part one: %d\n", one);
    assert(one == 2720);

    rewind(fp);
    int two = part_two(fp);
    printf("Part two: %d\n", two);
    assert(two == 71535);

    fclose(fp);
    return 0;
}