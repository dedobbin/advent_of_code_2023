#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <ctype.h>

typedef struct Numbers
{
    int a;
    int b;
} Numbers;

bool is_symbol(const char a)
{
    for (int i = '!'; i <= '@';i++){
        if (i == '.' || isdigit(i)){
            continue;
        } else if (a == i){
            return true;
        }
    }
    return false;
}

Numbers find_number(const char* const input, const int target)
{   
    // Because we use atoi we don't want + and - because atoi will see it as part of calc..
    char str[200];
    for (unsigned int i = 0; i < strlen(input) ;i++){
        if (input[i] == '+' || input[i] == '-'){
            str[i] = ',';
        } else {
            str[i] = input[i];
        }
    }

    // See if there is a digit at start. If so, there can be no seperate chars l and r
    int start = target;
    if (isdigit(str[start]) && start > 0){
        while(isdigit(str[start])){
            start--;
        }
        start++;
        int res = 0;
        res = atoi(str+start);
        return (Numbers){res, 0};
    }

    int l_res = 0;
    start = target - 1;
    if (start >= 0){
        while(isdigit(str[start])){
            start--;
        }

        start ++;
        l_res = atoi(str+start);
    }

    int r_res=0;
    start = target+1;
    r_res = atoi(input+start);
    return (Numbers){l_res, r_res};
}

int part_one(FILE* fp)
{
    unsigned int total = 0;
    char map[1024][1024] = {0};

    int h = 0;

    int y = 0;
    char line[200];
    while (fgets(line, sizeof(line), fp) != NULL){
        strcpy(map[y], line);
        y++;
    }
    h = y;

    for (int y = 0; y < h; y++){
        char* line = map[y];
        for (unsigned int x = 0; x < strlen(line);x++){
            if (is_symbol(line[x])){
                if (y > 0){
                    char* prev_line = map[y-1];
                    Numbers n = find_number(prev_line, x);
                    total += n.a + n.b;
                }
                if (y < h-1){
                    char* next_line = map[y+1];
                    Numbers n = find_number(next_line, x);
                    total += n.a + n.b;

                }
                Numbers n = find_number(line, x);
                total += n.a + n.b;            
            }
        }
    }
    return total;
}

int part_two(FILE* fp)
{
    int total = 0;
    char line[200];
    char lines[200][200];
    int y = 0;
    while(fgets(line, sizeof(line), fp) != NULL){
        strcpy(lines[y], line);  
        y++;
    }
    const int h = y;
    
    for (y = 0; y < h; y++){
        for (unsigned int x = 0; x < strlen(line); x++){
            if (lines[y][x] != '*'){
                continue;
            }
            int lines_to_scan[3];
            lines_to_scan[0] = y;
            int n_lines_to_scan = 1;
            int n_numbers_found = 0;
            int found_numbers[10] = {0};
            if (y-1 >= 0){
                lines_to_scan[n_lines_to_scan++] = y-1;
            }
            if (y+1 < h){
                lines_to_scan[n_lines_to_scan++] = y+1;
            }

            for (int j = 0; j< n_lines_to_scan; j++){
                int y_to_scan = lines_to_scan[j];
                Numbers res = find_number(lines[y_to_scan], x);
                if (res.a != 0){
                    found_numbers[n_numbers_found ++] = res.a;
                } 
                if (res.b != 0){
                    found_numbers[n_numbers_found ++] = res.b;
                }
            }

            // printf("Found %d numbers\n", n_numbers_found);
            // for (int i = 0; i < n_numbers_found; i++){
            //     printf("%d ", found_numbers[i]);
            // }
            // printf("\n");

            if (n_numbers_found == 2){
                total += found_numbers[0] * found_numbers[1];
            }
        }
        
    }
    return total;
}

int main(int argc, char **argv)
{
    char* input_path = argc > 1 ? argv[1] : "input.txt";
    FILE* fp = fopen(input_path, "r");
    if (!fp){
        perror("Could not open input file");
        exit(1);
    }

    int one = part_one(fp);
    printf("Part one: %d\n", one);
    assert(one == 530849);

    rewind(fp);
    int two = part_two(fp);
    printf("Part two: %d\n", two);
    assert(two == 84900879);
    
    fclose(fp);
    return 0;
}